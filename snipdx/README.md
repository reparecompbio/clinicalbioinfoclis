# SNiPDx (deprecated)

This analysis is deprecated and moved to the [nf-snipdx](https://bitbucket.org/reparecompbio/nf-snipdx/src/master/) pipeline

The historical codebase is stored here for reference.

## Workflow overview

![](../workflows/snipdx.PNG?raw=true)
